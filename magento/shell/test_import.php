<?php

require_once '../app/Mage.php';
require_once 'abstract.php';

class Mage_Shell_Test_New extends Mage_Shell_Abstract
{
    const ATTRIBUTE_CODE_COLOR = 'color';
    const ATTRIBUTE_CODE_SIZE = 'size';
    const ATTRIBUTE_CODE_PRICE = 'price';

    public function run()
    {
        Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

        $file = '../var/import/sample/import.csv';
        $csv = new Varien_File_Csv();
        $data = $csv->getData($file);

//        $simpleProductArr = array();
        foreach ($data as $value) {

            /**
             * $value[0] => sku
             * $value[1] => product type(simple,configurable, etc..)
             * $value[2] => product name
             * $value[3] => category id
             * $value[4] => product description
             * $value[5] => product short description
             * $value[6] => product weight
             * $value[7] => product price
             * $value[8] => product qty
             * $value[9] => product path to images
             * $value[10] => product path to small_image
             * $value[11] => product path to thumbnail
             * $value[12] => product color option id
             * $value[13] => product attribute set id
             * $value[14] => product visibility
             * $value[15] => product status
             * $value[16] => product size_id
             */

            /**
             * return function to getting the attribute color id
             */
            $attributeDetails = Mage::getSingleton("eav/config")->getAttribute('catalog_product', self::ATTRIBUTE_CODE_COLOR);
            $attribute = $attributeDetails->getData();
            $attributeColorId = $attribute['attribute_id'];

            /**
             * return function to getting the attribute size id
             */
            $attributeDetails = Mage::getSingleton("eav/config")->getAttribute('catalog_product', self::ATTRIBUTE_CODE_SIZE);
            $attribute = $attributeDetails->getData();
            $attributeSizeId = $attribute['attribute_id'];

            if ($value[1] == 'simple') {
                $simpleProduct = Mage::getModel('catalog/product');
                try {

                    $simpleProduct
                        ->setWebsiteIds(array(1))
                        ->setAttributeSetId($value[13])
                        ->setTypeId($value[1])
                        ->setCreatedAt(strtotime('now'));

                    $simpleProductSku = $simpleProduct->getSku();
                    if ($simpleProductSku !== $value[0]) {
                        $simpleProduct->setSku($value[0]);
                    }

                    $simpleProduct->setName($value[2])
                        ->setWeight($value[6])
                        ->setStatus($value[15])
                        ->setTaxClassId(0)
                        ->setVisibility($value[14])//not individually visibility
                        ->setSize($value[16])
                        ->setColor($value[12])
                        ->setNewsFromDate('')
                        ->setNewsToDate('')
                        ->setPrice($value[7])
                        ->setDescription($value[4])
                        ->setShortDescription($value[5])
                        ->setCategoryIds($value[3]);

                    $simpleProduct->setStockData(array(
                            'use_config_manage_stock' => 0,
                            'manage_stock' => 1,
                            'min_sale_qty' => 1,
                            'max_sale_qty' => 2,
                            'is_in_stock' => 1,
                            'qty' => $value[8]
                        )
                    );

                    /**
                     * $this = Mage_Shell_Test_New
                     */

                    if ($simpleProduct->getResource()->save($simpleProduct)) {
                        $this->saveImages($simpleProduct, $value[9], $value[10], $value[11]);
                        echo 'Simple Product ' . $simpleProduct->getName() . ' ' . $simpleProduct->getId() . ' imported successfully' . PHP_EOL;
                    }

                    $productId = $simpleProduct->getId();

                    $simpleProductArr[] = $productId;
                } catch (Exception $e) {
                    Mage::log($e->getMessage());
                    echo $e->getMessage();
                }

            }


            /**
             * Configurable Product Insert Section
             */
            if (count($simpleProductArr) > 0 && $value[1] == 'configurable') {
                $configProduct = Mage::getModel('catalog/product');
                try {
                    $configProduct
                        ->setWebsiteIds(array(1))
                        ->setAttributeSetId($value[13])
                        ->setTypeId($value[1])//product type
                        ->setCreatedAt(strtotime('now'));

                    $configProductSku = $configProduct->getSku();
                    if ($configProductSku !== $value[0]) {
                        $configProduct->setSku($value[0]);
                    }

                    $configProduct->setName($value[2])
                        ->setWeight($value[6])
                        ->setStatus($value[15])
                        ->setTaxClassId(0)
                        ->setVisibility($value[14])//catalog and search visibility
                        ->setSize($value[16])
                        ->setColor($value[12])
                        ->setPrice($value[7])
                        ->setDescription($value[4])
                        ->setShortDescription($value[5])
                        ->setCategoryIds($value[3]);

                    $configProduct->setStockData(array(
                            'use_config_manage_stock' => 0, //'Use config settings' checkbox
                            'manage_stock' => 1,
                            'min_sale_qty' => 1,
                            'max_sale_qty' => 2,
                            'is_in_stock' => 1,
                            'qty' => $value[8]
                        )
                    );

                    /**
                     * $this = Mage_Shell_Test_New
                     */

                    $simpleProducts = Mage::getResourceModel('catalog/product_collection')
                        ->addIdFilter($simpleProductArr)
                        ->addAttributeToSelect(self::ATTRIBUTE_CODE_COLOR)
                        ->addAttributeToSelect(self::ATTRIBUTE_CODE_SIZE)
                        ->addAttributeToSelect(self::ATTRIBUTE_CODE_PRICE);

                    $configProduct->setCanSaveConfigurableAttributes(true);
                    $configProduct->setCanSaveCustomOptions(true);
                    $configProduct->getTypeInstance()->setUsedProductAttributeIds(array($attributeColorId, $attributeSizeId)); //attribute ID of attribute 'color' in my store

                    $configurableAttributesData = $configProduct->getTypeInstance()->getConfigurableAttributesAsArray();
                    $configProduct->setCanSaveConfigurableAttributes(true);
                    $configProduct->setConfigurableAttributesData($configurableAttributesData);
                    $configurableProductsData = array();

                    foreach ($simpleProducts as $simple) {
                        $productData = array(
                            'label' => $simple->getAttributeText(self::ATTRIBUTE_CODE_COLOR),
                            'attribute_id' => $attributeColorId,
                            'value_index' => (int)$simple->getColor(),
                            'pricing_value' => $simple->getPrice()
                        );

                        $configurableProductsData[$simple->getId()] = $productData;
                        $configurableAttributesData[0]['values'][] = $productData;

                        $productData = array(
                            'label' => $simple->getAttributeText(self::ATTRIBUTE_CODE_SIZE),
                            'attribute_id' => $attributeSizeId,
                            'value_index' => (int)$simple->getSize(),
                            'pricing_value' => $simple->getPrice()
                        );

                        $configurableProductsData[$simple->getId()] = $productData;
                        $configurableAttributesData[1]['values'][] = $productData;
                    }

                    $configProduct->setConfigurableProductsData($configurableProductsData);
                    $configProduct->setConfigurableAttributesData($configurableAttributesData);
                    $configProduct->setCanSaveConfigurableAttributes(true);

                    if ($configProduct->getResource()->save($configProduct)) {
                        $this->saveImages($configProduct, $value[9], $value[10], $value[11]);
                        echo 'Configurable Product ' . $configProduct->getName() . ' ' . $configProduct->getId() . ' imported successfully' . PHP_EOL;
                    }

                    $configId = $configProduct->getId();

                    /**
                     * saving the configurable option attribute price value
                     */
                    if ($configProduct->getId() != '') {
                        $configurable = Mage::getModel('catalog/product')->load($configId);

                        $simpleProducts = Mage::getResourceModel('catalog/product_collection')
                            ->addIdFilter($simpleProductArr)
                            ->addAttributeToSelect(self::ATTRIBUTE_CODE_COLOR)
                            ->addAttributeToSelect(self::ATTRIBUTE_CODE_SIZE)
                            ->addAttributeToSelect(self::ATTRIBUTE_CODE_PRICE);

                        $configurableProductsData = array();
                        $configurableAttributesData = $configurable->getTypeInstance()->getConfigurableAttributesAsArray();

                        foreach ($simpleProducts as $simple) {
                            $productData = array(
                                'label' => $simple->getAttributeText(self::ATTRIBUTE_CODE_COLOR),
                                'attribute_id' => $attributeColorId,
                                'value_index' => (int)$simple->getColor(),
                                'pricing_value' => $simple->getPrice()
                            );

                            $configurableProductsData[$simple->getId()] = $productData;
                            $configurableAttributesData[0]['values'][] = $productData;

                            $productData = array(
                                'label' => $simple->getAttributeText(self::ATTRIBUTE_CODE_SIZE),
                                'attribute_id' => $attributeSizeId,
                                'value_index' => (int)$simple->getSize(),
                                'pricing_value' => $simple->getPrice()
                            );

                            $configurableProductsData[$simple->getId()] = $productData;
                            $configurableAttributesData[1]['values'][] = $productData;
                        }

                        $configurable->setConfigurableProductsData($configurableProductsData);
                        $configurable->setConfigurableAttributesData($configurableAttributesData);
                        $configurable->setCanSaveConfigurableAttributes(true);


                        $configurable->getResource()->save($configurable);
                        $simpleProductArr = array();
                        echo "SKU:" . $value[0] . ' added sucessfully' . PHP_EOL;
                    }
                } catch (Exception $e) {
                    $simpleProductArr = array();
                    Mage::log($e->getMessage());
                    echo "SKU:" . $value[0] . ' added unsucessfully' . PHP_EOL;
                }

            }
        }
    }

    public function saveImages($product, $baseImage, $smallImage, $thumbnail)
    {
        $productId = $product->getId();

        /**
         * for remove existing Images
         */
        $loadProduct = Mage::getModel('catalog/product')->load($productId);
        $mediaApi = Mage::getModel("catalog/product_attribute_media_api");
        $mediaApiItems = $mediaApi->items($loadProduct->getId());

        foreach ($mediaApiItems as $item) {
            $temporaryData = $mediaApi->remove($loadProduct->getId(), $item['file']);
        }
        $loadProduct->save(); //before adding need to save product

        $directory = Mage::getBaseDir('media') . DS . 'luxinten/';

        $images = array(
            'image' => $baseImage,
            'small_image' => $smallImage,
            'thumbnail' => $thumbnail,
        );

        foreach ($images as $imageType => $imageFileName) {
            $pathToFile = $directory . $imageFileName;
            if (file_exists($pathToFile)) {
                try {
                    $product->addImageToMediaGallery($pathToFile, $imageType, false);
                } catch (Exception $e) {
                    echo $e->getMessage();
                }
            } else {
                echo "Can not find image by path: `{$pathToFile}`<br/>";
            }
        }
        $product->getResource()->save($product);
    }

}

$shell = new Mage_Shell_Test_New();
$shell->run();